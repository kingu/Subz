import { Pipe, PipeTransform } from '@angular/core';
import { ISubscription } from '../Interfaces/subscriptionInterface';
import { NextBillingPipe } from './next-billing.pipe';
import { NextCancelationPeriodDeadlinePipe } from './next-cancelation-period-deadline.pipe';
import { CostByBillingIntervalPipe } from './cost-by-billing-interval.pipe';
import { billingIntervals } from '../BILLING_INTERVALS';

@Pipe({
  name: 'sortSubscriptions',
  pure: false
})
export class SortSubscriptionsPipe implements PipeTransform {
  availableBillingIntervals = billingIntervals;

  constructor(
    private costByBillingIntervalPipe: CostByBillingIntervalPipe,
    private nextBillingPipe: NextBillingPipe,
    private nextCancelationPeriodDeadlinePipe: NextCancelationPeriodDeadlinePipe) {}

  transform(subscriptions: ISubscription[], filterBy: string): ISubscription[] {
    if (!subscriptions) { return null; }
    if (!filterBy) { return subscriptions; }

    switch (filterBy) {
      case 'nameAsc': {
        return subscriptions.sort(this.sortByNameAsc);
      }
      case 'nameDesc': {
        return subscriptions.sort(this.sortByNameDesc);
      }
      case 'costAsc': {
        return subscriptions.sort((a, b) => {
          if (this.costByBillingIntervalPipe.transform(a, this.availableBillingIntervals[0]) <
          this.costByBillingIntervalPipe.transform(b, this.availableBillingIntervals[0])){
            return -1;
          }
          if (this.costByBillingIntervalPipe.transform(a, this.availableBillingIntervals[0]) >
          this.costByBillingIntervalPipe.transform(b, this.availableBillingIntervals[0])){
            return 1;
          }
          return 0;
        });
      }
      case 'costDesc': {
        return subscriptions.sort((a, b) => {
          if (this.costByBillingIntervalPipe.transform(a, this.availableBillingIntervals[0]) >
          this.costByBillingIntervalPipe.transform(b, this.availableBillingIntervals[0])){
            return -1;
          }
          if (this.costByBillingIntervalPipe.transform(a, this.availableBillingIntervals[0]) <
          this.costByBillingIntervalPipe.transform(b, this.availableBillingIntervals[0])){
            return 1;
          }
          return 0;
        });
      }
      case 'nextBillingAsc': {
        return subscriptions.sort((a, b) => {
          if (this.nextBillingPipe.transform(a).inDaysFromToday < this.nextBillingPipe.transform(b).inDaysFromToday){
            return -1;
          }
          if (this.nextBillingPipe.transform(a).inDaysFromToday > this.nextBillingPipe.transform(b).inDaysFromToday){
            return 1;
          }
          return 0;
        });
      }
      case 'nextBillingDesc': {
        return subscriptions.sort((a, b) => {
          if (this.nextBillingPipe.transform(a).inDaysFromToday > this.nextBillingPipe.transform(b).inDaysFromToday){
            return -1;
          }
          if (this.nextBillingPipe.transform(a).inDaysFromToday < this.nextBillingPipe.transform(b).inDaysFromToday){
            return 1;
          }
          return 0;
        });
      }
      case 'nextContractExtensionAsc': {
        return subscriptions.sort((a, b) => {
          if (!this.nextCancelationPeriodDeadlinePipe.transform(a) || !this.nextCancelationPeriodDeadlinePipe.transform(b)) { return 1; }

          if (this.nextCancelationPeriodDeadlinePipe.transform(a).inDaysFromToday < this.nextCancelationPeriodDeadlinePipe.transform(b).inDaysFromToday){
            return -1;
          }
          if (this.nextCancelationPeriodDeadlinePipe.transform(a).inDaysFromToday > this.nextCancelationPeriodDeadlinePipe.transform(b).inDaysFromToday){
            return 1;
          }
          return 0;
        });
      }
      case 'nextContractExtensionDesc': {
        return subscriptions.sort((a, b) => {
          if (!this.nextCancelationPeriodDeadlinePipe.transform(a) || !this.nextCancelationPeriodDeadlinePipe.transform(b)) { return 1; }

          if (this.nextCancelationPeriodDeadlinePipe.transform(a).inDaysFromToday > this.nextCancelationPeriodDeadlinePipe.transform(b).inDaysFromToday){
            return -1;
          }
          if (this.nextCancelationPeriodDeadlinePipe.transform(a).inDaysFromToday < this.nextCancelationPeriodDeadlinePipe.transform(b).inDaysFromToday){
            return 1;
          }
          return 0;
        });
      }
      default: {
        return subscriptions;
      }
    }
  }

  sortByNameAsc(a: ISubscription, b: ISubscription) {
    if (a.name.toLowerCase() < b.name.toLowerCase()){
      return -1;
    }
    if (a.name.toLowerCase() > b.name.toLowerCase()){
      return 1;
    }
    return 0;
  }

  sortByNameDesc(a: ISubscription, b: ISubscription) {
    if (a.name.toLowerCase() < b.name.toLowerCase()){
      return 1;
    }
    if (a.name.toLowerCase() > b.name.toLowerCase()){
      return -1;
    }
    return 0;
  }

}
