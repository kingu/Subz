* Improvement: Select date format for new subscriptions in settings
* Improvement: About dialog now shows version, contact and issue tracker
* Improvement: Added DKK to currencies
* Improvement: Modified app icon and loading spinner colors
* Improvement: Exit the app by using the back button
* Improvement: Allow feature dates for subscriptions
* Improvement: Data restore now offers merging with current data
* Fix: Sort subscriptions by next contract extension not triggered in some cases
* Fix: Non localized cancel-button within currency selection
* Fix: Sorting of new added subscriptions
* Fix: Wrong cancellation period deadline for subscriptions in some cases